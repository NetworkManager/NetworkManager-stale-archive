#!/bin/bash

set -e

OLD_IFS="$IFS"
SCRIPT_NAME="$(readlink -f "$0")"

TAG="${TAG-}"
REMOTE="${REMOTE:-origin}"
REMOTE_GIT="${REMOTE_GIT-$(git remote get-url --push "$REMOTE")}"
REMOTE_BACKUP_GIT="${REMOTE_BACKUP_GIT-git@gitlab.freedesktop.org:NetworkManager/NetworkManager-stale-archive.git}"
VERBOSE="${VERBOSE-0}"
DELETE_REF_REMOTE="${DELETE_REF_REMOTE-0}"
DELETE_REF_LOCAL="${DELETE_REF_LOCAL-0}"
ONLY_CLEAN_LOCAL="${ONLY_CLEAN_LOCAL-0}"
ONLY_CLEAN_REMOTE_BACKUP="${ONLY_CLEAN_REMOTE_BACKUP-0}"

IFS=' '
EXCLUDE_DELETE_REF_REMOTE=( $EXCLUDE_DELETE_REF_REMOTE )
IFS="$OLD_IFS"

show_conf() {
    printf '%s\n' "${1}REMOTE=\"$REMOTE\""
    printf '%s\n' "${1}REMOTE_GIT=\"$REMOTE_GIT\""
    printf '%s\n' "${1}REMOTE_BACKUP_GIT=\"$REMOTE_BACKUP_GIT\""
    printf '%s\n' "${1}TAG=\"$TAG\""
    printf '%s\n' "${1}VERBOSE=\"$VERBOSE\""
    printf '%s\n' "${1}DELETE_REF_REMOTE=\"$DELETE_REF_REMOTE\""
    printf '%s\n' "${1}DELETE_REF_LOCAL=\"$DELETE_REF_LOCAL\""
    printf '%s\n' "${1}ONLY_CLEAN_LOCAL=\"$ONLY_CLEAN_LOCAL\""
    printf '%s\n' "${1}ONLY_CLEAN_REMOTE_BACKUP=\"$ONLY_CLEAN_REMOTE_BACKUP\""
    printf '%s\n' "${1}EXCLUDE_DELETE_REF_REMOTE=\"${EXCLUDE_DELETE_REF_REMOTE[*]}\""
}

usage() {
    printf 'usage: %s [--tag TAG | --auto-tag] [ -v | --verbose ] [ --only-clean-local | --only-clean-remote-backup ] [ -x EXCLUDE_DELETE_REF_REMOTE ... ]\n' "$0"
    printf '\n'
    printf '  environment:\n'
    show_conf '    '
    printf '\n'
    printf 'This script is to backup/archive the git-refs from our main repository.\n'
    printf 'It fetches all refs from $REMOTE_GIT and pushes them to $REMOTE_BACKUP_GIT\n'
    printf 'It remaps those refs to refs/archive/$TAG/*\n'
    printf 'Afterwards it deletes all the heads from $REMOTE_GIT, except certain protected\n'
    printf 'branches like `main`\n'
    printf '\n'
    printf '  ONLY_CLEAN_LOCAL: only delete refs refs/archive/$TAG/* in local repository\n'
    printf '  ONLY_CLEAN_REMOTE_BACKUP: only delete refs refs/archive/$TAG/* in $REMOTE_BACKUP_GIT repository\n'
    printf '  EXCLUDE_DELETE_REF_REMOTE: a space separated list of refs that should not be deleted from $REMOTE_BACKUP_GIT repository\n'
    printf '\n'
    printf 'Usage:\n'
    printf '\n'
    printf ' # start with a NetworkManager checkout:\n'
    printf ' $ git clone git@gitlab.freedesktop.org:NetworkManager/NetworkManager.git\n'
    printf ' $ cd NetworkManager\n'
    printf '\n'
    printf ' # export a tag (for repeated use)\n'
    printf ' $ export TAG=\"nm-$(date '\''+%%Y%%m%%d-%%H%%M%%S'\'')\"\n'
    printf '\n'
    printf ' # run script once, to fetch all refs and see what it would\n'
    printf ' $ %s\n' "$SCRIPT_NAME"
    printf '\n'
    printf ' # Consider whether some branches should not be removed from remote.\n'
    printf ' # Use $EXCLUDE_DELETE_REF_REMOTE or -x option.\n'
    printf ' # If all looks good, rerun with\n'
    printf ' $ EXCLUDE_DELETE_REF_REMOTE="refs/heads..." DELETE_REF_REMOTE=1 DELETE_REF_LOCAL=1 %s\n' "$SCRIPT_NAME"
}

die() {
    printf '%s\n' "$@" >&2
    exit 1
}

cmd() {
    printf 'EXEC: %s\n' "$*"
    if [ "$VERBOSE" = 1 ]; then
        "$@"
    else
        "$@" &>/dev/null
    fi
}

_auto_tag() {
    printf '%s' "nm-$(date '+%Y%m%d-%H%M%S')"
}

is_protected_ref() {
    local ref="$1"
    local a
    shift

    for a; do
        [ "$ref" = "$a" ] && return 0
    done

    case "$ref" in
        refs/heads/main | \
        refs/heads/nm-1-* | \
        refs/heads/systemd | \
        refs/heads/automation) \
            ;;
        refs/heads/* )
            # we actually only delete branches. Other refs (tags, notes) are kept.
            return 1
            ;;
        *)
            ;;
    esac

    return 0
}

CUR_DIR="$(readlink -f .)"
GIT_DIR="$(git rev-parse --show-toplevel)"

ARGV=( "$@" )
i=0
while test "$i" -lt ${#ARGV[@]} ; do
    C="${ARGV[$i]}"
    i=$((i+1))
    case "$C" in
        --auto-tag)
            TAG="$(_auto_tag)"
            ;;
        --tag)
            C="${ARGV[$i]}"
            i=$((i+1))
            test -n "$C" || die "missing argument to --tag option"
            TAG="$C"
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        --only-clean-local)
            ONLY_CLEAN_LOCAL=1
            ;;
        --only-clean-remote-backup)
            ONLY_CLEAN_REMOTE_BACKUP=1
            ;;
        -x)
            C="${ARGV[$i]}"
            i=$((i+1))
            test -n "$C" || die "missing argument to -x option"
            EXCLUDE_DELETE_REF_REMOTE+=("$C")
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            usage
            echo
            die "invalid parameter \"$C\""
            ;;
    esac
done

if test -z "$TAG" ; then
    usage
    die "Missing tag. Use --auto-tag (or better export \$TAG). Tags are commonly in the form \"$(_auto_tag).\""
fi

re='^[a-zA-Z0-9._-]+$'
[[ "$TAG" =~ $re ]] || die "Invalid tag \"$TAG\""

show_conf ''

cd "$GIT_DIR"

if [ "$ONLY_CLEAN_REMOTE_BACKUP" == 1 ] ; then
    IFS=$'\n'
    REFS_LINES=( $(git ls-remote --refs "$REMOTE_BACKUP_GIT") )
    IFS="$OLD_IFS"

    ARGS=()
    for line in "${REFS_LINES[@]}"; do
        SHA="${line%%	*}"
        REF="${line#*	}"

        [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

        echo ">>> [$SHA]    [$REF]"
        ARGS+=("--force-with-lease=$REF:$SHA" ":$REF")
    done
    if [ ${#ARGS[@]} -gt 0 ]; then
        cmd git push "$REMOTE_BACKUP_GIT" "${ARGS[@]}"
    fi
    exit 0
fi

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    cmd git fetch "$REMOTE_GIT" \
        --no-tags \
        --prune \
        --force \
        "refs/*:refs/archive/$TAG/refs/*" \
        "^refs/archive/*"
fi

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    cmd git push "$REMOTE_BACKUP_GIT" \
        --no-tags \
        --prune \
        --force \
        "refs/archive/$TAG/*:refs/archive/$TAG/*"
fi

IFS=$'\n'
REFS_LINES=( $(git show-ref) )
IFS="$OLD_IFS"

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    for line in "${REFS_LINES[@]}"; do
        SHA="${line%% *}"
        REF="${line#* }"

        [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

        re='^refs/archive/([^/]+)/(.*)$'
        [[ "$REF" =~ $re ]] || die bug
        REMOTE_REF="${BASH_REMATCH[2]}"

        [ "$REF" = ${BASH_REMATCH[0]} ] || die bug
        [ "$TAG" = ${BASH_REMATCH[1]} ] || die bug
        [ "$REF" = "refs/archive/$TAG/$REMOTE_REF" ] || die bug

        DEL_REMOTE=0
        is_protected_ref "$REMOTE_REF" "${EXCLUDE_DELETE_REF_REMOTE[@]}"|| DEL_REMOTE=1

        CMD=(git push "$REMOTE_GIT" --force-with-lease="$REMOTE_REF:$SHA" ":$REMOTE_REF")
        if [ "$DEL_REMOTE" = 1 ]; then
            if [ "$DELETE_REF_REMOTE" = 1 ]; then
                cmd "${CMD[@]}"
            else
                echo "# would delete remote:   ${CMD[@]}"
            fi
        else
            echo "# keep protected remote: ${CMD[@]}"
        fi

    done
fi

for line in "${REFS_LINES[@]}"; do
    SHA="${line%% *}"
    REF="${line#* }"

    [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

    CMD=(git update-ref -d "$REF" "$SHA")
    if [ "$DELETE_REF_LOCAL" = 1 ]; then
        cmd "${CMD[@]}"
    else
        echo "# would delete local refs: ${CMD[@]}"
    fi
done

